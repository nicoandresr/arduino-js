var five = require('johnny-five');
var board = new five.Board();

board.on('ready', function() {
  var moisture = new five.Sensor({
    pin: 'A0',
    freq: 1000,
    threshold: 5
  });

  moisture.on('change', function() {
    console.log(this.scaleTo(0, 100));
  });
});
